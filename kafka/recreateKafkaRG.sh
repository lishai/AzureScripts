#!/bin/bash
GRP="${1:-Kafka}"
echo "deleting resource group $GRP:"
az group delete --name "$GRP" 
az group create --name "$GRP" --location "Central US" #\
  #&& az network vnet create -n vnet2 -g "$GRP" --address-prefixes 10.0.0.0/24 --subnet-name subnet2
