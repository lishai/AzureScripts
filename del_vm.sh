#!/bin/bash
set -o pipefail -o nounset
# die() - print error message and exit (with error)
function die() {
  echo "$@" >&2
  exit 2
}

# usage() - print usage and exit (with error)
function usage() {
  echo "usage: " >&2
  echo >&2
  echo "$0 -g <resource_group_name> -n <vm_name>" >&2
  echo "-- or --" >&2
  echo "$0 --resource-group <resource_group_name> --name <vm_name>" >&2
  echo >&2
  exit 3
}

########################
#   parase arguments   #
########################
vm_name=""
rg_name=""
while [[ $# -gt 0 ]]; do
  key="$1"
  case "$key" in 
    -n|--name)			vm_name="$2"	;;
    -g|--resource-group)	rg_name="$2"	;;
    *)				
      echo "unknown parameter '$key'" >&2 
      usage		
      ;;
  esac
  shift
  shift
done
if [ -z "$vm_name" -o -z "$rg_name" ]; then
  usage
fi

vm_path="/resourceGroups/$rg_name/providers/Microsoft.Compute/virtualMachines/$vm_name"
DISKS=$( az disk list -g "$rg_name" -o tsv --query "[?not_null(managedBy) && ends_with(managedBy,'$vm_path')].name" 
)
 

echo Deleting vm $rg_name/$vm_name:
az vm delete -g "$rg_name" -n "$vm_name" -y \
      || echo FIXME die "az vm delete failed"

for disk in $DISKS; do 
  echo "Deleting disk '$disk'."
  az disk delete -g "$rg_name" -n "$disk" --no-wait -y
done

nic="${vm_name}_nic1"
pubIp="${vm_name}_publicIp1"

echo "Deleting NIC '$nic:"
az network nic delete -g "$rg_name" -n "$nic"  

echo "Deleting public ip '$pubIp':"
az network public-ip delete -g "$rg_name" -n "${vm_name}_publicIp1"

