#!/bin/bash

# die() - print error message and exit (with error)
function die() {
  echo "$@" >&2
  exit 2
}

# usage() - print usage and exit (with error)
function usage() {
  die "usage: $0 --num-ssd-disks=<number> [ --storage-account=<storage_account_name> --share=<share_name> --key=<access_key> ]"
}

########################
#   parase arguments   #
########################
for i in "$@"; do
  case $i in
    --num-ssd-disks=*)
      SSD_DISKS="${i#*=}"  
      ;;
    --storage-account=*)
      STORAGE_ACCOUNT="${i#*=}"
      ;;
    --share=*)
      SHARE="${i#*=}"
      ;;
    --key=*)
      KEY="${i#*=}"
      ;;
  esac
done

# number of ssh disks is required
if ! [ -n "$SSD_DISKS"  -a "$SSD_DISKS" -ge 0 ] 2>/dev/null; then
  usage
fi

# if any of the parameters storage-account,share and key are given, then all 
# of them are required
if [ -n "$STORAGE_ACCOUNT" -o -n "$SHARE" -o -n "$KEY" ]; then
  if [ -z "$STORAGE_ACCOUNT" -o -z "$SHARE" -o -z "$KEY" ]; then
    usage
  fi
fi


########################### 
#   data disk functions   #
########################### 

# prep_data_disk(lun_id, disk_type):
# 
# partition data disk, format and add it to fstab with mount 
# point /data/ssd<lun_id> or /data/hdd<lun_id>
function prep_data_disk() {
  local lun_id="$1"; shift
  local disk_type="$1"; shift
  
  [ -n "$lun_id" ] || die "usage: prep_data_disk <lun_id> <SSD|HDD>"
  [ "$disk_type" == 'SSD' -o "$disk_type" == 'HDD' ] || die "usage: prep_data_disk <lun_id> <SSD|HDD>"
  local lun_dev="/dev/disk/azure/scsi1/lun$lun_id"
  local part_dev="${lun_dev}-part1"
  [ -b "$lun_dev" ] || die "Internal Error: '$lun_dev' does not exist or is not a block device" >&2
  if [ -b "$part_dev" ]; then
    echo
    echo "skipping $lun_dev, it is already partitioned"
    echo
    continue
  fi
  local mount_point=$( echo "/data/${disk_type}${lun_id}" | tr 'A-Z' 'a-z' )
  local label=$(printf 'DATA_%02d_%s' $lun_id $disk_type)
  
  if grep -q "$label" /etc/fstab; then
    die "ERROR: label '$label' already in fstab. Aborting."
  fi
  if grep -q "$mount_point" /etc/fstab; then
    die "ERROR: mount point '$mount_point' already in fstab. Aborting"
  fi
  
  sudo parted --script $lun_dev mklabel gpt
  sudo parted --script $lun_dev -a optimal mkpart primary xfs 1 100%
  udevadm settle
  sudo mkfs.xfs -q -L "$label" "$part_dev"
  sudo mkdir -p "$mount_point"
  printf 'LABEL=%-15s %-15s xfs defaults,noatime,nodiratime,nofail    0 0\n' "$label" "$mount_point" \
    | sudo tee -a /etc/fstab
}

# prep_all_data_disks() - partition, format and add to fstab all data disks 
#   using prep_data_disk() 
function prep_all_data_disks() {
  for lun_id in $(ls /dev/disk/azure/scsi1/lun* | grep 'lun[0-9]*$' | grep -o '[0-9]*$' | sort -n); do
    if [ "$lun_id" -lt "$SSD_DISKS" ]; then
      prep_data_disk "$lun_id" "SSD"
    else
      prep_data_disk "$lun_id" "HDD"
    fi
  done  
}


##################################
#   shared CIFS mount function   #
##################################
function mount_cifs_share() {
  local mount_point='/mnt/shared'
  sudo mkdir -p $mount_point
  
  if grep -q $mount_point /etc/fstab; then
    die "ERROR: mount point '$mount_point' already in fstab. Aborting"
  fi
  sudo bash -c 'cat > /etc/cifs.cred' <<-EOF
	username=$STORAGE_ACCOUNT
	password=$KEY
	EOF
  sudo chmod 600 /etc/cifs.cred
  sudo chown root:root /etc/cifs.cred
  local mount_opts='_netdev,nofail,vers=2.1,serverino,mfsymlinks'
  mount_opts="$mount_opts,dir_mode=0775,file_mode=0775,uid=hadmin,gid=hadmin,credentials=/etc/cifs.cred"
  local share_path="//$STORAGE_ACCOUNT.file.core.windows.net/$SHARE"
  echo "$share_path   $mount_point   cifs   $mount_opts   0 0" | sudo tee -a /etc/fstab
}

############
#   main   #
############
prep_all_data_disks
if [ -n "$STORAGE_ACCOUNT" ]; then
  mount_cifs_share
fi
sudo mount -a

